import 'package:flutter/material.dart';

class ResultPage extends StatelessWidget {
  String status;
  String result;
  ResultPage({Key? key, required this.status, required this.result}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF023020),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF023020),
        title: Text('Result'.toUpperCase()),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(result, style: TextStyle(fontSize: 150, fontWeight: FontWeight.bold, color: Colors.white),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Status: ', style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.white),),
                      Text(status, style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Colors.white),),
                    ],
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: ()=>Navigator.pop(context, 'reset'),
              child: Container(
                width: double.maxFinite,
                height: 80,
                decoration: BoxDecoration(
                    color: Colors.teal,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(50),
                        topLeft: Radius.circular(50))),
                child: Center(
                    child: Text(
                      'RE-CALCULATE',
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 24,
                          color: Colors.white),
                    )),
              ),
            ),
          ],
        ),

      ),
    );
  }
}
