import 'dart:math';

class Bmi {
  int height;
  int weight;
  double? result;

  Bmi({required this.height, required this.weight,});

  double bmiResult(){
    result = (weight / pow( height / 100 , 2));

    return result!;
  }

  String getBmiStatus(){
    if(result! <= 18.4){
      return 'Underweight';
    } else if(result! < 25){
      return 'Normal';
    } else if(result! < 40){
      return 'Overweight';
    } else {
      return 'Obese';
    }
  }

}
